from Tkinter import *
class LabelDemo( Frame ):
    def __init__(self):
        Frame.__init__(self)
        self.pack(expand = YES, fill = BOTH)
        self.master.title("ciag fibonacciego")

        self.button = Button(self, text="Oblicz",
                             command=self.fib, width=16, height=1)
        self.button.pack(side=BOTTOM)

        self.Label2 = Label(self, text="Dla 6:")
        self.Label2.pack(side=TOP)
        self.Label1 = Label(self, text="Wynik=")
        self.Label1.pack()

    def fib(self):
        a = 0
        b = 1
        n = 6
        for x in range(0, n):
            b += a
            a = b - a
            print a
            self.Label1['text']+=str(a)+','

def main():
    LabelDemo().mainloop()

if __name__ == "__main__":
    main()
