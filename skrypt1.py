'''
skrypt1
'''
#komentarz
a = 'zmienna'
b = 4
#print(a + b)
a,b,c = 4,8,9
print a,b,c
a = 0o5
print a
b = 0xabc
print b
c = 3+5j
print c
d = 4.6
print d

napis='To jest\' string'
print napis

napis2="Test z tabulatorem\ti znakiem\n nowego wiersza"
print napis2

napis3='''wiersz o
wielu
wierszach'''
print napis3

print "zielone"+"jablko"

print "B"+"a"*5+"rdzo pyszne!"
print "Py" "thon"

a=5;b=8
print a,b

#konwersja liczby na napis
a=4
b='65'
c=`a`+b
print c

'''
    ZAJECIA 2
'''

'''
Typy liczb
'''

#Dlugie liczby calkowite
print 4*7L

#Liczby rzeczywiste
print 2.5
print 2e+4
print 4.0/3.0
print 5.0//2.0 #podloga
print 3.5e+4+1000000L*2

#Liczby zespolone
print -2+3j
print 3j**2
print 3.2+400L/(2+2j)

'''
Zmiana wartosci zmiennych
'''

a,b,c,d,e = 1,3,7,4,6
a+=2
b-=2
c*=2
d/=2
print a,b,c,d,e

'''
czytanie znakow z klawiatury
'''

'''
imie = raw_input("Jak masz na imie?\n")
print imie
'''

'''
Napisy zaawansowane
'''

napis="Wiek: "+str(18)
print napis
print napis.replace("W","w")
print napis.lower()
print napis.upper()
napis="Ta liczba %f to %s" % (3.23, "liczba")
print napis
print '{0}, {1}, {2}'.format('a','b','c')

'''
Listy i krotki
'''

l=[1,2,'element',3.14]
print l
k=(1,2,'element',3.14)
print k

print l[1]
print k[1]

print l[1:3]
print k[:-2]

print 1 in l

l[0]=3
print l

#k[0]=3
#print(k)

l[1:3]=['a','b']
print l

'''
Listy wielowymiarowe
'''

macierz=[1,[1,2,3,4]]
print macierz

print macierz[1][3]

macierz[1][3]=5
print macierz

'''
Listy - modyfikowanie
'''

lista=[1,2,3]
lista2=lista+[4,5,6]
print lista2

lista=[4,6,2,4,8]
lista.sort()
print lista

lista.reverse()
print lista

lista.append(6)
print lista

lista.insert(2,10)
print lista

lista.pop()
print lista

lista.remove(4)
print lista

del lista[1:3]
print lista

'''
Slowniki
'''

slownik={'a':'b','klucz':15,3:[1,2,3,4]}
print slownik['a']
print len(slownik)

print list(slownik.keys())

d={}
#print 'b' in d

d['b']=1
print d

del slownik[3]
print slownik

slownik.clear()
print slownik

'''
Boolean
'''

a=True
b=False
print a,b
x=4
y=8

print x<y
print x==y
print x!=y

print a or b
print a and b
print not a
print not b

'''
Opertaory arytmetyczne
'''

a,b=2,3

print a+b
print a-b
print a*b
print a/b
print a%b
print a**b #potega

'''
Instrukcje warunkowe i petle
'''

a=6
a=5
if a<4:
    print a
elif a>4:
    print b

for x in range (-10,11):
    print "%+i" % x,

print "\n"

for x in range(5,100,10):
    print "%3i%6o%5x" % (x,x,x)

for x in range(5,100,10):
    print "%-3i%#-6o%#-5x" % (x,x,x)

for x in range(5,100,10):
    print "%3i %#04o %#04x" % (x,x,x)

a=[1,2,3,4,5,6]
while a:
    a=a[:len(a)-1]
    print a
