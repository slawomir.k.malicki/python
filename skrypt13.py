"""PY63"""
from Tkinter import *
class LabelDemo( Frame ):
    def __init__(self):
        Frame.__init__(self)
        self.pack(expand = YES, fill = BOTH)
        self.master.title("przyklad")

        self.button = Button(self, text="QUIT", fg="red",
                             command=self.quit1, width=16, height=1)
        self.hi_there = Button(self, text="Hej", command=self.say_hi,
                               width=16, height=1)

        self.Label1 = Label(self, text="Etykieta tekstowa")
        self.Label2 = Label(self, text="Etykieta tekstowa z ikona")
        self.Label3 = Label(self, bitmap = "warning")

        self.button.pack(side=BOTTOM)
        self.hi_there.pack(side=BOTTOM)
        self.Label1.pack()
        self.Label2.pack(side=LEFT)
        self.Label3.pack(side=LEFT)

    def quit1(self):
        print "Koniec"
        quit()

    def say_hi(self):
        print "Hej - witojcie"

def main():
    LabelDemo().mainloop()

if __name__ == "__main__":
    main()