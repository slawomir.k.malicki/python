'''
operacje na plikach i katalogach
'''

from os import *

#sprawdzenie w jakim katalogu obecnie jestesmy
print getcwd()

#zmiana biezacego katalogu na inny
#chdir('Test')
print getcwd()

#zawartosc katalogu
print listdir('..')

#zawartosc katalogu - sciezka absolutna
print listdir(r'E:\Dokumenty\python')

#filtrowanie plikow i katalogow wg okreslonego wzorca

import fnmatch
print fnmatch.fnmatch('Python', 'P*n')
print fnmatch.fnmatch('Python', 'P*e')

#lista plikow z rozszerzeniem py
print [x for x in listdir(r'C:\Users\slawo\PycharmProjects\python')
      if fnmatch.fnmatch(x,'*.py')]

#listy plikow konczacych sie liczba 6 lub 1
print [x for x in listdir(r'C:\Users\slawo\PycharmProjects\python')
       if fnmatch.fnmatch(x,'*[61].*')]

import glob
for x in glob.glob(r'C:\Users\slawo\PycharmProjects\python\*[61].*'):
    print x

#rozdzielenie sciezki absolutnej na katalog w ktorym znajduje sie plik oraz
print path.split('C:\Users\slawo\PycharmProjects\python\skrypt10.py')

for x in glob.glob(r'..\*[61].*'):
    print path.split(x)[1]

#laczenie ciagu katalogow w sciezke
print path.join('/','C:','Users','slawo','PycharmProjects','python')
print path.join(r'C:\Users','slawo\PycharmProjects','python')

#sprawdzenie czy podana sciezka jest absolutna
print path.isabs(r'..\skryp10.py')
print path.isabs(r'C:\Users\slawo\PycharmProjects\python\skrypt10.py')

#sprawdzenie czy dany obiekt dyskowy istnieje
print path.exists('C:\Users\slawo\PycharmProjects\python\skrypt10.py')
print path.exists('C:\Users\slawo\PycharmProjects\python')

#zmiana nazwy katalogu lub pliku
#rename('../Test','../nTest')
print path.exists('nTest')
print listdir('.')

#sprawdzenie czy dany obiekt dyskowy jest plikiem
print path.isfile('nTest')
print path.isfile('skrypt10.py')

#sprawdzenie czy dany obiekt dyskowy jest katalogiem
print path.isdir('nTest')
print path.isdir('skrypt10.py')

#sprawdzenie czy dany obiekt dyskowy jest dyskiem
print path.ismount('nTest')
print path.ismount('C:/')

#sprawdzenie dlugosci pliku (bajty)
print path.getsize('skrypt10.py')
print path.getsize('nTest')

for x in listdir('.'):
    print x, path.getsize(x)

#czas stworzenia pliku
from time import ctime
print ctime(path.getctime('skrypt10.py'))

#czas ostatniej modyfikacji
print ctime(path.getmtime('skrypt10.py'))

#rekursywne przechodzenie katalogow
for sciezka, podkatalogi, pliki in walk(r'./'):
    print "W katalogu %s znajduje sie %i bajtow w %i plikach" \
        % (sciezka, sum(path.getsize(path.join(sciezka, nazwa))
                        for nazwa in pliki), len(pliki))