import random

with open('hasla.txt','rU') as f:
    listaHasel = [line.rstrip('\n') for line in f]
lUczestnikow = int(raw_input("Podaj liczbe uczestnikow:"))
uczestnicy = []
dostepneCyfry = list(range(1, lUczestnikow+1))
randomHaslo = random.choice(listaHasel)
kategoria = randomHaslo.split(':')[0]
haslo = randomHaslo.split(':')[1]
pokazHaslo = "*" * len(haslo)
odkrywaneHaslo = haslo

class Gracz:
    def __init__(self, lUczestnikow, imie):
        self.lUczestnikow = lUczestnikow
        self.imie = imie
        self.kolejnosc = 0
        self.lPunktow = 0
        self.wylosowanaCyfra = 0
    def losujKolejnosc(self, listaCyfr):
        self.kolejnosc = random.choice(listaCyfr)
        return self.kolejnosc
    def losujCyfre(self):
        self.wylosowanaCyfra = random.randint(-10,10)
        if self.wylosowanaCyfra == 0:
            print "Bankrut! Strata kolejki"
            self.lPunktow = 0
        else:
            print "wylosowano: %s " % self.wylosowanaCyfra
        return self.wylosowanaCyfra
    def podajLitere(self):
        koniec = False
        litera = raw_input("Podaj litere:\n")
        global haslo
        global odkrywaneHaslo
        global pokazHaslo
        if litera in odkrywaneHaslo:
            iloscLiter = len(odkrywaneHaslo)
            for l in [i for i, letter in enumerate(odkrywaneHaslo) if letter == litera]:
                pokazHaslo = pokazHaslo[0:l] + litera + pokazHaslo[l+1:]
            odkrywaneHaslo = odkrywaneHaslo.replace(litera,"")
            iloscLiter = iloscLiter - len(odkrywaneHaslo)
            self.lPunktow += self.wylosowanaCyfra*iloscLiter
            print "Podana litera wystepuje w hasle %s razy" % iloscLiter
            print pokazHaslo
            zgaduj = raw_input("Podaj haslo:\n")
            if zgaduj == haslo:
                koniec = True
                print "Brawo, odgadles haslo!"
                self.lPunktow *= len(odkrywaneHaslo)
            else:
                print "Podane haslo jest nieprawidlowe."
        else:
            print "Podana litera nie wystepuje w hasle."
        return koniec

for x in range(lUczestnikow):
    uczestnicy.append(Gracz(lUczestnikow,"gracz"+str(x+1)))
    if len(dostepneCyfry) > 0:
        dostepneCyfry.remove(uczestnicy[x].losujKolejnosc(dostepneCyfry))
uczestnicy.sort(key = lambda x: x.kolejnosc, reverse = True)
print "Kolejnosc:"
for x in uczestnicy:
    print x.imie
print "Wylosowano kategorie: %s" % kategoria
idx = 0;
while(True):
    x = uczestnicy[idx]
    idx = (idx + 1) % len(uczestnicy)
    print x.imie + ":"
    if x.losujCyfre() != 0:
        if x.podajLitere():
            print "Koniec gry. Wygrywa %s" % x.imie
            break
        else:
            print "Nastepny gracz."
    else:
        print "Nastepny gracz"

uczestnicy.sort(key = lambda x: x.lPunktow, reverse = True)
import sys
sys.stdout = open("wyniki.txt","wb")
print "Ranking".center(64, '*')
for x in uczestnicy:
    print x.imie + ":" + str(x.lPunktow)
