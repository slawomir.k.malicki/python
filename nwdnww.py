def NWD(a,b):
    c=0
    while b!=0:
        c=a%b
        a=b
        b=c
    return a

def NWW(a,b):
    x=a
    y=b
    c = 0
    while b != 0:
        c = a % b
        a = b
        b = c
    return x/a*y

print NWD(4,8)
print NWW(4,8)