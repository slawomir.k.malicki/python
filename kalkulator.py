from Tkinter import *
class Kalkulator( Frame ):
    def __init__(self):
        Frame.__init__(self)
        self.pack(expand = YES, fill = BOTH)
        self.master.title("Kalkulator")

        self.Dzialanie = Label(self, text="")
        self.Wynik = Label(self, text="Wynik:")
        self.d1 = Button(self, text="1", command=lambda:self.liczba(1), width=3, height=1)
        self.d2 = Button(self, text="2", command=lambda:self.liczba(2), width=3, height=1)
        self.d3 = Button(self, text="3", command=lambda:self.liczba(3), width=3, height=1)
        self.d4 = Button(self, text="4", command=lambda:self.liczba(4), width=3, height=1)
        self.d5 = Button(self, text="5", command=lambda:self.liczba(5), width=3, height=1)
        self.d6 = Button(self, text="6", command=lambda:self.liczba(6), width=3, height=1)
        self.d7 = Button(self, text="7", command=lambda:self.liczba(7), width=3, height=1)
        self.d8 = Button(self, text="8", command=lambda:self.liczba(8), width=3, height=1)
        self.d9 = Button(self, text="9", command=lambda:self.liczba(9), width=3, height=1)
        self.d0 = Button(self, text="0", command=lambda:self.liczba(0), width=3, height=1)

        self.plus = Button(self, text="+", command=lambda:self.operand('+'), width=3, height=1)
        self.minus = Button(self, text="-", command=lambda:self.operand('-'), width=3, height=1)
        self.divide = Button(self, text="/", command=lambda:self.operand('/'), width=3, height=1)
        self.times = Button(self, text="*", command=lambda:self.operand('*'), width=3, height=1)
        self.equals = Button(self, text="=", width=3, height=1)

        self.Dzialanie.pack(side=TOP)
        self.Wynik.pack(side=TOP)
        self.d1.pack(side=LEFT)
        self.d2.pack(side=LEFT)
        self.d3.pack(side=LEFT)
        self.d4.pack(side=LEFT)
        self.d5.pack(side=LEFT)
        self.d6.pack(side=LEFT)
        self.d7.pack(side=LEFT)
        self.d8.pack(side=LEFT)
        self.d9.pack(side=LEFT)
        self.d0.pack(side=LEFT)

        self.plus.pack(side=BOTTOM)
        self.minus.pack(side=BOTTOM)
        self.divide.pack(side=BOTTOM)
        self.times.pack(side=BOTTOM)
        self.equals.pack(side=BOTTOM)

    def liczba(self, i):
        self.Dzialanie['text']+=str(i)
    def operand(self, op):
        self.Dzialanie['text'] += op
    # def oblicz(self):
    #     for x in self.Dzialanie.cget('text'):
    #         if x != '+' and x != '-' and x != '*' and x != '/':
    #             a = x
    #         elif x



def main():
    Kalkulator().mainloop()

if __name__ == "__main__":
    main()