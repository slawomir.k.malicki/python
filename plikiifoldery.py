from os import *

sciezka = raw_input('Podaj sciezke absolutna katalogu: ')

print 'Czy sciezka istnieje?: ' +  str(path.exists(sciezka))
chdir(sciezka)
print 'Jestesmy tutaj: ' + getcwd()

pliki = []
foldery = []

for x in listdir('.'):
    if path.isfile(x):
        pliki.append(x)
    elif path.isdir(x):
        foldery.append(x)

print 'Ilosc plikow:'.center(64, '*')
print len(pliki)
print 'Ilosc folderow:'.center(64, '*')
print len(foldery)

print 'Pliki i ich rozmiary:'.center(64, '*')
for x in pliki:
    print x, path.getsize(x), 'B', round(path.getsize(x)/1024.0,3), 'KB', round((path.getsize(x)/1024.0)/1024.0,4), 'MB'

print 'Foldery i ich rozmiary:'.center(64, '*')
for x in foldery:
    print x, path.getsize(x), 'B', round(path.getsize(x)/1024.0,3), 'KB', round((path.getsize(x)/1024.0)/1024.0,4), 'MB'

total_size = 0
for dirpath, dirnames, filenames in walk('.'):
    for f in filenames:
        fp = path.join(dirpath, f)
        total_size += path.getsize(fp)
print 'Rozmiar katalogu: '.center(64,'*')
print total_size

