class Adres:
    def __init__(self, imie, nazwisko, miasto, ulica, nrDomu, kod, telefon):
        self.imie = imie
        self.nazwisko = nazwisko
        self.miasto = miasto
        self.ulica = ulica
        self.nrDomu = nrDomu
        self.kod = kod
        self.telefon = telefon
    def wyswietl(self):
        print 'Imie: %s' % self.imie
        print 'Nazwisko: %s' % self.nazwisko
        print 'Miasto: %s' % self.miasto
        print 'Ulica: %s' % self.ulica
        print 'NrDomu: %s' % self.nrDomu
        print 'Kod: %s' % self.kod
        print 'Telefon: %s' % self.telefon
    def zmienImie(self,imie2):
        self.imie = imie2
    def zmienNazwisko(self,nazwisko2):
        self.nazwisko = nazwisko2
    def zmienMiasto(self,miasto2):
        self.miasto = miasto2
    def zmienUlica(self,ulica2):
        self.ulica = ulica2
    def zmienKod(self,kod2):
        self.kod = kod2
    def zmienNrDomu(self,nrDomu2):
        self.nrDomu = nrDomu2
    def zmienTelefon(self,telefon2):
        self.telefon = telefon2

class KsiazkaAdresowa:
    ksiazka = {}
    def __init__(self, ksiazka):
        self.ksiazka = ksiazka
    def dodajKontakt(self, pesel, kontakt):
        self.ksiazka[pesel] = kontakt
    def usunKontakt(self, pesel):
        try:
            self.ksiazka.pop(pesel)
        except:
            print 'Blad. Sprawdz czy podales poprawny pesel'
    def wyswietlKontakt(self,pesel):
        try:
            self.ksiazka[pesel].wyswietl()
        except:
            print 'Blad. Sprawdz czy podales poprawny pesel'
    def wyswietlWszystkich(self):
        for x in self.ksiazka.itervalues():
            print x.wyswietl()
            print ''.center(64, '*')
ksiazkaAdr = {}
pesel = '94072605698'
slawek = Adres('Slawomir','Malicki','Lublin','slawkowa 1','23','22-520','098765432')
ash = Adres('Ash','Ketchum','Alabastia','trenerska','10','30-120','0123456789')
ksiazkaAdr = {pesel:slawek}
ksiazkaAdr['1'] = ash
adresy = KsiazkaAdresowa(ksiazkaAdr)


op = -1
while op != 6:
    op = int(raw_input('Jaka operacje chcesz wykonac:\n1:Dodaj kontakt\n2:Usun kontakt\n3:Wyswietl kontakt\n4:Zmien dane kontaktu\n5:Wyswietl wszystkie kontakty\n6:Zakoncz\n'))
    if op == 1:
        pesel = raw_input('Podaj pesel:')
        imie = raw_input('Podaj imie:')
        nazwisko = raw_input('Podaj nazwisko:')
        miasto = raw_input('Podaj miasto:')
        ulica = raw_input('Podaj ulice:')
        nrDomu = raw_input('Podaj nr domu:')
        kod = raw_input('Podaj kod pocztowy:')
        tel = raw_input('Podaj nr telefonu:')
        kontakt = Adres(imie,nazwisko,miasto,ulica,nrDomu,kod,tel)
        adresy.dodajKontakt(pesel,kontakt)
    elif op == 2:
        pesel = raw_input('Podaj pesel osoby, ktora chcesz usunac z ksiazki adresowej:')
        adresy.usunKontakt(pesel)
    elif op == 3:
        pesel = raw_input('Podaj pesel osoby, ktorej dane chcesz wyswietlic:')
        adresy.wyswietlKontakt(pesel)
    elif op == 4:
        pesel = raw_input('Podaj pesel osoby, ktorej dane chcesz zmienic:')
        if pesel in adresy.ksiazka:
            dane = raw_input('Jakie dane chcesz zmienic? Podaj po przecinku odpowiednie numery:\n1:Imie\n2:Nazwisko\n3:Miasto\n4:Ulica\n5:Nr domu\n6:Kod pocztowy\n7:Nr telefonu\n').split(',')
            for x in dane:
                if x == '1':
                    nImie = raw_input('Podaj nowe imie:')
                    adresy.ksiazka[pesel].zmienImie(nImie)
                if x == '2':
                    nNazwisko = raw_input('Podaj nowe nazwisko:')
                    adresy.ksiazka[pesel].zmienNazwisko(nNazwisko)
                if x == '3':
                    nMiasto = raw_input('Podaj nowe miasto:')
                    adresy.ksiazka[pesel].zmienMiasto(nMiasto)
                if x == '4':
                    nUlica = raw_input('Podaj nowa ulice:')
                    adresy.ksiazka[pesel].zmienUlica(nUlica)
                if x == '5':
                    nNrDomu = raw_input('Podaj nowy nr domu:')
                    adresy.ksiazka[pesel].zmienNrDomu(nNrDomu)
                if x == '6':
                    nKod = raw_input('Podaj nowy kod pocztowy:')
                    adresy.ksiazka[pesel].zmienKod(nKod)
                if x == '7':
                    nTel = raw_input('Podaj nowy nr telefonu:')
                    adresy.ksiazka[pesel].zmienTelefon(nTel)
        else:
            print 'Blad. Sprawdz czy podales poprawny pesel'
    elif op == 5:
        adresy.wyswietlWszystkich()
    elif op == 6:
        break
    else:
        print 'Zly numer operacji'


